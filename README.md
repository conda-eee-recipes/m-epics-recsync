m-epics-recsync conda recipe
============================

Home: https://bitbucket.org/europeanspallationsource/m-epics-recsync

Package license: EPICS Open License

Recipe license: BSD 2-Clause

Summary: EPICS recsync module
